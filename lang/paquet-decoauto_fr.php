<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'decoauto_description' => '',
	'decoauto_nom' => 'Déconnexion Automatique',
	'decoauto_slogan' => '',
);
