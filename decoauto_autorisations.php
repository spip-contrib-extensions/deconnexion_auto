<?php
/**
 * Définit les autorisations du plugin Déconnexion Automatique
 *
 * @plugin     Déconnexion Automatique
 * @copyright  2019
 * @author     tofulm
 * @licence    GNU/GPL
 * @package    SPIP\Decoauto\Autorisations
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Fonction d'appel pour le pipeline
 * @pipeline autoriser */
function decoauto_autoriser() {
}
